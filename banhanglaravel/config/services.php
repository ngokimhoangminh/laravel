<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    
    'facebook' => [
        'client_id' => '440490013821125',  //client face của bạn
        'client_secret' => '7810ffe2d729c55d258e3b0c0361f05f',  //client app service face của bạn
        'redirect' => 'https://hoangminh.net:8080/myweb/banhanglaravel/admin/callback
' //callback trả về
    ],

    'google' => [
        'client_id' => '1040688528444-t2ratmnkih3crqsh1go8f1nt23ifeiub.apps.googleusercontent.com',
        'client_secret' => '2cMiOaNypkZSgmkPGGbF0fbP',
        'redirect' => 'http://localhost:8080/emc_lab/banhanglaravel/google/callback' 
    ],

];
