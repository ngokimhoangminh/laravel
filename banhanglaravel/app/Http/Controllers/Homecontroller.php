<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Session;
use Mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class Homecontroller extends Controller
{
    //p
   	public function index(Request $request)
   	{
   		// $category_product=DB::table('tbl_category_product')->where('category_status','1')->orderBy('category_id','desc')->get();
    	// $brand_product=DB::table('tbl_brand')->where('brand_status','1')->orderBy('brand_id','desc')->get();
    	//$list_product=DB::table('tbl_product')->where('product_status','1')->orderBy('product_id','desc')->limit(6)->get();
    	
    	//Model
      $meta_des="Chuyên bán những mặt hàng thời trang cho mọi lứa tuổi, phụ kiện cho gia đình, trang phục cho mẹ và bé ";
      $meta_title="Trang phục thời trang gia đình cho mọi lứa tuổi";
      $meta_keywords="thoi trang gia dinh, thời trang gia đình, phụ kiện thời trang";
      $url_canonical=$request->url();

    	$category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
    	$brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
    	$list_product=Product::where('product_status','1')->orderBy('product_id','desc')->limit(6)->get();
   		return view('pages.home')->with('result_category',$category_product)->with('result_brand',$brand_product)->with('result_product',$list_product);

      // ->with('meta_des',$meta_des)->with('meta_title',$meta_title)->with('meta_keywords',$meta_keywords)->with('url_canonical',$url_canonical);

       

      //return view('pages.home')->with(compact('category_product','brand_product','list_product','meta_des','meta_title','meta_keywords','url_canonical'));
   	}

    public function search(Request $request)
    {
        $keywords=$request->keywords;
       
         $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
       
        $search_product=Product::where('product_name','like','%'.$keywords.'%')->where('product_status','1')->get();       
        return view('pages.product.search')->with('result_category',$category_product)->with('result_brand',$brand_product)->with('result_search',$search_product);
    }
    
    public function send_mail()
    {
      //send mail
        $to_name ="hoang minh cp10";
        $to_email ="hoangminhcp10@gmail.com";//send to this email

        $data = array("name"=>"Mail từ tài khoản khách hàng","body"=>'Mail gửi về vấn đề hàng hóa'); //body of mail.blade.php

        Mail::send('pages.send_mail',$data,function($message) use ($to_name,$to_email){
        $message->to($to_email)->subject('Test thử gửi mail Google');//send this mail with subject
        $message->from($to_email,$to_name);//send from this mail
        });
        return redirect('/')->with('message','');
      //--send mail
    }
}
