<?php

namespace App\Http\Controllers;
use Cart;//composer require bumbummen99/shoppingcart
use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class CartController extends Controller
{
    //
    public function add_to_cart(Request $request)
    {
    	$productid=$request->product_id_hidden;
    	$quantity=$request->quantity;

    	$cart_product=DB::table('tbl_product')->where('product_id',$productid)->first();

    	$data['id']=$cart_product->product_id;
    	$data['qty']=$quantity;
    	$data['name']=$cart_product->product_name;
    	$data['price']=$cart_product->product_price;
    	$data['weight']=$cart_product->product_price;
    	$data['options']['image']=$cart_product->product_image;
        Cart::add($data);
    	return Redirect::to('/show_cart');
    }
    public function show_cart()
    {
    	$category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
    	return view('pages.cart.cart_content')->with('result_category',$category_product)->with('result_brand',$brand_product);
    }
    public function delete_cart($rowId)
    {
    	Cart::update($rowId, 0);
    	return Redirect::to('/show_cart');
    }
    public function update_quantity_cart(Request $request)
    {
    	$rowId=$request->rowid_cart;
    	$qty=$request->cart_quantity;
    	Cart::update($rowId, $qty);
    	return Redirect::to('/show_cart');
    }

    public function add_cart_ajax(Request $request)
    {
        $data=$request->all();
        $session_id=substr(md5(microtime()),rand(0,26),5);
        $cart=Session::get('cart');
        if($cart==true)
        {
            $is_avaiable=0;
            foreach ($cart as $key => $val) {
                # code...
                if($val->product_id==$data['cart_product_id'])
                {
                    $is_avaiable++;
                }
            }
            if($is_avaiable==0)
            {
                $cart[]=array(
                'session_id'=> $session_id,
                'product_id'=> $data['cart_product_id'],
                'product_name'=> $data['cart_product_name'],
                'product_price'=> $data['cart_product_price'],
                'product_image'=> $data['cart_product_image'],
                'product_qty'=> $data['cart_product_qty'],
                );
            }
            Session::put('cart',$cart);
            
        }else
        {
            $cart[]=array(
                'session_id'=> $session_id,
                'product_id'=> $data['cart_product_id'],
                'product_name'=> $data['cart_product_name'],
                'product_price'=> $data['cart_product_price'],
                'product_image'=> $data['cart_product_image'],
                'product_qty'=> $data['cart_product_qty'],
            );

        }
        Session::put('cart',$cart);
        Session::save();
    }
}
