<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class CategoryProduct extends Controller
{
    //
    public function check_login()
    {
        $admin_id=Session::get('admin_id');
        if($admin_id)
        {
             return Redirect::to('/dashboard');
        }else
        {
            return Redirect::to('/admin')->send();
        }
    }
    public function add_category_product()
    {
        $this->check_login();
    	return view('admin.add_category_product');

    }
    public function list_category_product()
    {
        $this->check_login();
    	//$list_category_product=DB::table('tbl_category_product')->get();//lấy dữ liệu trong bảng tblcategory
        $list_category_product=Category::all();
    	$category_product=view('admin.list_category_product')->with('result',$list_category_product);
    	//with=render dữ liệu từ controller xuống view
    	//return view('admin_layout')->with('admin.list_category_product',$category_product);
    	return $category_product;
    }
    public function save_category_product(Request $request)
    {
        $this->check_login();
    	// $data=array();
    	// $data['category_name']=$request->category_product_name;
    	// $data['category_des']=$request->category_product_des;
    	// $data['category_status']=$request->category_product_status;

    	// DB::table('tbl_category_product')->insert($data);

        $data=$request->all();
        $category=new Category();
        $category->category_name=$data['category_product_name'];
        $category->category_des=$data['category_product_des'];
        $category->meta_keywords=$data['meta_category_keys'];
        $category->category_status=$data['category_product_status'];
        $category->save();

    	Session::put('message','Thêm danh mục sản phẩm thành công');
    	return Redirect::to('/list_category_product');//đường dẫn
    }
    public function unactive_status_category($cat_productid)
    {
        $this->check_login();
    	//DB::table('tbl_category_product')->where('category_id',$cat_productid)->update(['category_status'=>0]);
        $category=Category::find($cat_productid);
        $category->category_status=0;
        $category->save();
    	Session::put('message','Đã chuyển sang Ẩn thành công');
    	return Redirect::to('/list_category_product');
    }
    public function active_status_category($cat_productid)
    {
        $this->check_login();
    	//DB::table('tbl_category_product')->where('category_id',$cat_productid)->update(['category_status'=>1]);

        $category=Category::find($cat_productid);
        $category->category_status=1;
        $category->save();
    	Session::put('message','Đã chuyển sang Hiện thành công');
    	return Redirect::to('/list_category_product');
    }
    public function update_category_product($cat_productid)
    {
    	//$update_category_product=DB::table('tbl_category_product')->where('category_id',$cat_productid)->get();//lấy dữ liệu trong bảng tblcategory


        $update_category_product=Category::find($cat_productid);
        //dùng find thì không dùng foreach
    	$category_product=view('admin.update_category_product')->with('result_update',$update_category_product);
    	//render dữ liệu từ controller xuống view
    	return $category_product;
    }
    public function edit_category_product(Request $request,$cat_productid)
    {
        $this->check_login();
    	// $data=array();
    	// $data['category_name']=$request->category_product_name;
    	// $data['category_des']=$request->category_product_des;
    	// DB::table('tbl_category_product')->where('category_id',$cat_productid)->update($data);

        $data=$request->all();
        $category=Category::find($cat_productid);
        $category->category_name=$data['category_product_name'];
        $category->category_des=$data['category_product_des'];
        $category->meta_keywords=$data['meta_category_keywords'];
        $category->save();
    	Session::put('message','Đã cập nhật danh mục thành công');
    	return Redirect::to('/list_category_product');
    }
    public function delete_category_product($cat_productid)
    {
        $this->check_login();
    	//DB::table('tbl_category_product')->where('category_id',$cat_productid)->delete();
        $category=Category::find($cat_productid);
        $check_category=DB::table('tbl_product')->where('tbl_product.category_id',$cat_productid)->first();

        if($check_category)
        {
            Session::put('message','Không thể xóa vì đang có sản phẩm thuộc danh mục này');
            return Redirect::to('/list_category_product');
        }else
        {
            $category->delete();
            Session::put('message','Đã xóa danh mục thành công');
            return Redirect::to('/list_category_product');
        }
        
    }
    public function category_home(Request $request,$categoryid)
    {
        // $category_product=DB::table('tbl_category_product')->where('category_status','1')->orderBy('category_id','desc')->get();
        // $brand_product=DB::table('tbl_brand')->where('brand_status','1')->orderBy('brand_id','desc')->get();
        // $category_home=DB::table('tbl_product')->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')->where('tbl_product.category_id',$categoryid)->where('tbl_product.product_status','1')->get();


        //model
        $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();

        $category_home=Product::join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')->where('tbl_product.category_id',$categoryid)->where('tbl_product.product_status','1')->get();

        //  foreach ($category_home as $key => $val) {
        //      # code...
        //          $meta_des=$val->category_des;
        //        $meta_title=$val->category_name;
        //        $meta_keywords=$val->meta_keywords;
        //        $url_canonical=$request->url();
        // }

        $category_name=Category::find($categoryid);

        return view('pages.category.category_home')->with('result_category',$category_product)->with('result_brand',$brand_product)->with('rs_category_home',$category_home)->with('result_category_name',$category_name);

        // ->with('meta_des',$meta_des)->with('meta_title',$meta_title)->with('meta_keywords',$meta_keywords)->with('url_canonical',$url_canonical)
    }
       public function brand_home($brandid)
    {
        // $category_product=DB::table('tbl_category_product')->where('category_status','1')->orderBy('category_id','desc')->get();
        // $brand_product=DB::table('tbl_brand')->where('brand_status','1')->orderBy('brand_id','desc')->get();
        // $brand_home=DB::table('tbl_product')->join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')->where('tbl_product.brand_id',$brandid)->where('tbl_product.product_status','1')->get();

        //model
        $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
        $brand_home=Product::join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')->where('tbl_product.brand_id',$brandid)->where('tbl_product.product_status','1')->get();

        $brand_name=Brand::where('brand_id',$brandid)->limit(1)->get();

        return view('pages.brand.brand_home')->with('result_category',$category_product)->with('result_brand',$brand_product)->with('rs_brand_home',$brand_home)->with('result_brand_name',$brand_name);
    }
}
