<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Brand;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class BrandProduct extends Controller
{
    //
    public function check_login()
    {
        $admin_id=Session::get('admin_id');
        if($admin_id)
        {
             return Redirect::to('/dashboard');
        }else
        {
            return Redirect::to('/admin')->send();
        }
    }
    public function add_brand_product()
    {
        $this->check_login();
    	return view('admin.add_brand_product');

    }
    public function list_brand_product()
    {
        $this->check_login();
    	//$list_brand_product=DB::table('tbl_brand')->get();
        //take(4)==limit(4)
        $list_brand_product=Brand::orderBy('brand_id','desc')->get();
    	$brand_product=view('admin.list_brand_product')->with('result_brand',$list_brand_product);
    	return $brand_product;
    }
    public function save_brand_product(Request $request)
    {
        $this->check_login();
    	// $data=array();
    	// $data['brand_name']=$request->brand_product_name;
    	// $data['brand_des']=$request->brand_product_des;
    	// $data['brand_status']=$request->brand_product_status;
    	// DB::table('tbl_brand')->insert($data);

        $data=$request->all();
        $brand=new Brand();//new cho phép insert dữu liệu
        $brand->brand_name=$data['brand_product_name'];
        $brand->brand_des=$data['brand_product_des'];
        $brand->brand_status=$data['brand_product_status'];
        $brand->save();

    	Session::put('message','Thêm thương hiệu sản phẩm thành công');
    	return Redirect::to('/list_brand_product');//đường dẫn
    }
    public function unactive_status_brand($brand_productid)
    {
        $this->check_login();

    	//DB::table('tbl_brand')->where('brand_id',$brand_productid)->update(['brand_status'=>0]);
    	$brand=Brand::find($brand_productid);
        $brand->brand_status=0;
        $brand->save();
        Session::put('message','Đã chuyển sang Ẩn thành công');
    	return Redirect::to('/list_brand_product');
    }
    public function active_status_brand($brand_productid)
    {
        $this->check_login();
    	//DB::table('tbl_brand')->where('brand_id',$brand_productid)->update(['brand_status'=>1]);
        $brand=Brand::find($brand_productid);
        $brand->brand_status=1;
        $brand->save();
    	Session::put('message','Đã chuyển sang Hiện thành công');
    	return Redirect::to('/list_brand_product');
    }
    public function update_brand_product($brand_productid)
    {
        $this->check_login();
    	//$update_brand_product=DB::table('tbl_brand')->where('brand_id',$brand_productid)->get();//lấy dữ liệu trong bảng tblcategory

        //find=where
        $update_brand_product=Brand::where('brand_id',$brand_productid)->get();

        //$update_brand_product=Brand::find($brand_productid);

    	$brand_product=view('admin.update_brand_product')->with('result_update',$update_brand_product);
    	//render dữ liệu từ controller xuống view
    	return $brand_product;
    }
    public function edit_brand_product(Request $request,$brand_productid)
    {
        $this->check_login();
    	// $data=array();
    	// $data['brand_name']=$request->brand_product_name;
    	// $data['brand_des']=$request->brand_product_des;
    	// DB::table('tbl_brand')->where('brand_id',$brand_productid)->update($data);

        $data=$request->all();
        $brand=Brand::find($brand_productid);//tìm ra 1 thương hiệu để update
        $brand->brand_name=$data['brand_product_name'];
        $brand->brand_des=$data['brand_product_des'];
        $brand->save();

    	Session::put('message','Đã cập nhật thương hiệu thành công');
    	return Redirect::to('/list_brand_product');
    }
    public function delete_brand_product($brand_productid)
    {
    	       $this->check_login();
            //DB::table('tbl_brand')->where('brand_id',$brand_productid)->delete();
            $brand=Brand::find($brand_productid);
            //find tìm kiếm dựa trên khóa chính
            $check_brand=DB::table('tbl_product')->where('tbl_product.brand_id',$brand_productid)->first();
            if($check_brand)
            {
                Session::put('message','Không thể xóa vì đang có sản phẩm thuộc thương hiệu này');
                return Redirect::to('/list_brand_product');
            }else
            {
                $brand->delete();
                Session::put('message','Đã xóa thương hiệu thành công');
                return Redirect::to('/list_brand_product');

            }
            
    }
}
