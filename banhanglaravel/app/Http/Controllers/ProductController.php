<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Comment;
use App\Models\Order;
use App\Models\Rating;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class ProductController extends Controller
{
    //
    public function check_login()
    {
        $admin_id=Session::get('admin_id');
        if($admin_id)
        {
             return Redirect::to('/dashboard');
        }else
        {
            return Redirect::to('/admin')->send();
        }
    }
     public function add_product()
    {
        $this->check_login();
        $category_product=Category::orderBy('category_id','desc')->get();
        $brand_product=Brand::orderBy('brand_id','desc')->get();
        return view('admin.add_product')->with('result_category',$category_product)->with('result_brand',$brand_product);

    }
    public function list_product()
    {
        $this->check_login();
        $list_product=Product::join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')
        ->orderBy('tbl_product.product_id','desc')->get();
        $manager_product=view('admin.list_product')->with('result_product',$list_product);
        return $manager_product;
    }
    public function save_product(Request $request)
    {
        $this->check_login();
        // $data=array();
        // $data['category_id']=$request->product_category;
        // $data['brand_id']=$request->product_brand;
        // $data['product_name']=$request->product_name;
        // $data['product_des']=$request->product_des;
        // $data['product_content']=$request->product_content;
        // $data['product_price']=$request->product_price;
        // $data['product_status']=$request->product_status;

        // $get_image=$request->file('product_image');
        // if($get_image)
        // {
        //  $get_name_image=$get_image->getClientOriginalName();
        //  $get_name_split=current(explode('.',$get_name_image));
        //  //current lấy chuỗi cắt đầu tiên
        //  $name_image=$get_name_split.rand(0,99).'.'.$get_image->getClientOriginalExtension();
        //  //getClientOriginalExtension()lấy đuôi ảnh
        //  $get_image->move('public/backend/uploads/product',$name_image);
        //  $data['product_image']=$name_image;
        //  DB::table('tbl_product')->insert($data);
        //  Session::put('message','Thêm sản phẩm thành công');
        //  return Redirect::to('/list_product');//đường dẫn

        // }
        // $data['product_image']='';
        // DB::table('tbl_product')->insert($data);

        //model
        $data=$request->all();
        $product=new Product();
        $product->category_id=$data['product_category'];
        $product->brand_id=$data['product_brand'];
        $product->product_name=$data['product_name'];
        $product->product_des=$data['product_des'];
        $product->product_content=$data['product_content'];
        $product->product_price=$data['product_price'];
        $product->product_status=$data['product_status'];

        $get_image=$request->file('product_image');
        if($get_image)
        {
            $get_name_image=$get_image->getClientOriginalName();
            $get_name_split=current(explode('.',$get_name_image));
            //current lấy chuỗi cắt đầu tiên
            $name_image=$get_name_split.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            //getClientOriginalExtension()lấy đuôi ảnh
            $get_image->move('public/backend/uploads/product',$name_image);
            $product->product_image=$name_image;
            $product->save();
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('/list_product');//đường dẫn

        }
        $product->product_image='';       
        $product->save();
        Session::put('message','Thêm sản phẩm thành công');
        return Redirect::to('/list_product');//đường dẫn
    }
    public function unactive_status_product($productid)
    {
        $this->check_login();
        //DB::table('tbl_product')->where('product_id',$productid)->update(['product_status'=>0]);
        $product=Product::find($productid);
        $product->product_status=0;
        $product->save();
        Session::put('message','Đã chuyển sang Ẩn thành công');
        return Redirect::to('/list_product');
    }
    public function active_status_product($productid)
    {
        $this->check_login();
        //DB::table('tbl_product')->where('product_id',$productid)->update(['product_status'=>1]);
        $product=Product::find($productid);
        $product->product_status=1;
        $product->save();
        Session::put('message','Đã chuyển sang Hiện thành công');
        return Redirect::to('/list_product');
    }
    public function update_product($productid)
    {
        $this->check_login();
        $category_product=Category::orderBy('category_id','desc')->get();
        $brand_product=Brand::orderBy('brand_id','desc')->get();
        
        $update_product=Product::find($productid);//lấy dữ liệu trong bảng tblcategory

        $_product=view('admin.update_product')->with('result_update',$update_product)->with('result_category',$category_product)->with('result_brand',$brand_product);
        //render dữ liệu từ controller xuống view
        return $_product;
    }
    public function edit_product(Request $request,$productid)
    {
        $this->check_login();
        // $data=array();
        // $data['category_id']=$request->product_category;
        // $data['brand_id']=$request->product_brand;
        // $data['product_name']=$request->product_name;
        // $data['product_des']=$request->product_des;
        // $data['product_content']=$request->product_content;
        // $data['product_price']=$request->product_price;

        // $get_image=$request->file('product_image');
        // if($get_image)
        // {
        //  $get_name_image=$get_image->getClientOriginalName();
        //  $get_name_split=current(explode('.',$get_name_image));
        //  //current lấy chuỗi cắt đầu tiên
        //  $name_image=$get_name_split.rand(0,99).'.'.$get_image->getClientOriginalExtension();
        //  //getClientOriginalExtension()lấy đuôi ảnh
        //  $get_image->move('public/backend/uploads/product',$name_image);
        //  $data['product_image']=$name_image;
        //  DB::table('tbl_product')->where('product_id',$productid)->update($data);
        //  Session::put('message','Cập nhật sản phẩm thành công');
        //  return Redirect::to('/list_product');//đường dẫn

        // }
        // DB::table('tbl_product')->where('product_id',$productid)->update($data);

        //model
        $data=$request->all();
        $product=Product::find($productid);
        $product->category_id=$data['product_category'];
        $product->brand_id=$data['product_brand'];
        $product->product_name=$data['product_name'];
        $product->product_des=$data['product_des'];
        $product->product_content=$data['product_content'];
        $product->product_price=$data['product_price'];
       

        $get_image=$request->file('product_image');
        if($get_image)
        {
            $get_name_image=$get_image->getClientOriginalName();
            $get_name_split=current(explode('.',$get_name_image));
            //current lấy chuỗi cắt đầu tiên
            $name_image=$get_name_split.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            //getClientOriginalExtension()lấy đuôi ảnh
            $get_image->move('public/backend/uploads/product',$name_image);
            $product->product_image=$name_image;
            $product->save();
            Session::put('message','Cập nhật sản phẩm thành công');
            return Redirect::to('/list_product');//đường dẫn

        }
          
        $product->save();
        Session::put('message','Cập nhật sản phẩm thành công');
        return Redirect::to('/list_product');//đường dẫn
    }
    public function delete_product($productid)
    {
        $this->check_login();
        //DB::table('tbl_product')->where('product_id',$productid)->delete();
        $product=Product::find($productid);
        $product->delete();
        //Unlink('public/backend/uploads/product/'.$productimage);
        Session::put('message','Đã xóa sản phẩm thành công');
        return Redirect::to('/list_product');
    }
    public function details_product($productid)
    {
        $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();

        $details_product=DB::table('tbl_product')
        ->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')
        ->where('tbl_product.product_id',$productid)->get();
        //sản phẩm liên quan trong chi tiết sản phẩm
        foreach($details_product as $key => $dl_value)
        {
            $brand_id=$dl_value->brand_id;
        }
        $rating=Rating::where('product_id',$productid)->avg('rating');
        $rating=Round($rating);
        $recommend_product=DB::table('tbl_product')
        ->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->join('tbl_brand','tbl_brand.brand_id','=','tbl_product.brand_id')
        ->where('tbl_brand.brand_id',$brand_id)->get();

        return view('pages.product.details_product')->with('result_category',$category_product)->with('result_brand',$brand_product)->with('result_dl_product',$details_product)->with('re_product',$recommend_product)->with('rating',$rating);
    }
    public function load_comment(Request $request)
    {
        $product_id=$request->product_id;
        $comemnt=Comment::where('comment_product_id',$product_id)->where('comment_parent_content','=',0)->where('comment_status',0)->orderBy('comment_id','DESC')->get();
        $comment_rep=Comment::with('product')->where('comment_parent_content','>',0)->orderBy('comment_id','DESC')->get();
        $output="";
        foreach ($comemnt as $key => $comm) {
            # code...
            $output.='
                <div class="row style_comment">
                    <div class="col-md-2">
                            
                            <a href="#"><img  class="img img-responsive img-thumbnail" src="'.url('/public/frontend/image/3.png').'"></a>
                    </div>
                    <div class="col-md-10">
                            <p style="color:green;">@'.$comm->comment_name.'</p>
                            <p style="color:black;">'.$comm->comment_date.'</p>
                            <p>'.$comm->comment.'</p>
                    </div>
                </div>
                <p></p>
                ';
                foreach ($comment_rep as $key => $com_reply) {
                    # code...
                    if($com_reply->comment_parent_content==$comm->comment_id){
                $output.='<div class="row style_comment" style="margin:5px 40px; background:ghostwhite;">
                    <div class="col-md-2">
                            
                            <a href="#"><img width="40%"  class="img img-responsive img-thumbnail" src="'.url('/public/frontend/image/avataradmin.png').'"></a>
                    </div>
                    <div class="col-md-10">
                            <p style="color:blue;">@'.$com_reply->comment_name.'</p>
                            <p style="color:black;">'.$com_reply->comment_date.'</p>
                            <p>'.$com_reply->comment.'</p>
                    </div>
                </div>
                <p></p>';
                }    
            }  

        }
        echo $output;
    }
    public function send_comment(Request $request,$productid)
    {


        // $product_id=$request->product_id;
        // $comment_name=$request->comment_name;
        // $comment_content=$request->comment_content;

        
        
        // $comment=new Comment();
        // $comment->comment=$comment_content;
        // $comment->comment_name=$comment_name;
        // $comment->comment_status=1;
        // $comment->comment_product_id=$product_id;
        // $comment->comment_parent_content=0;
        // $comment->save();
    
        
        $customer_id=Session::get('customer_id');

        $check_comment=Order::join('tbl_order_details','tbl_order.order_id','=','tbl_order_details.order_id')
        ->where('tbl_order_details.product_id',$productid)
        ->where('tbl_order.customer_id',$customer_id)
        ->select('tbl_order.*','tbl_order_details.*')
        ->first();

        if($customer_id)
        {
            if($check_comment)
            {
                $data_comment=$request->all();
                $com=new Comment();
                
                $com->comment=$data_comment['comment_content'];
                $com->comment_name=Session::get('customer_name');
                $com->comment_status=1;
                $com->comment_product_id=$productid;
                $com->comment_parent_content=0;

                $product_id=$data_comment['comment_product_id'];

                $com->save();
                Session::put('message','Thêm bình luận thành công, bình luận đang chờ duyệt');
                return Redirect::to('/details_product/'.$product_id);    
            }else
            {
                $data_comment=$request->all();
                $product_id=$data_comment['comment_product_id'];
                Session::put('message','Bạn chưa mua sản phẩm này không thể bình luận');
                return Redirect::to('/details_product/'.$product_id); 
                
            }
        }else
        {
            return Redirect::to('/checkout_login');
        }

       

        
    }
    public function insert_rating(Request $request)
    {
        $data=$request->all();
        $rating=new Rating();
        $rating->product_id=$data['product_id'];
        $rating->rating=$data['index'];
        $rating->save();
        echo 'success';
    }
}
