<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Order;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class CheckoutController extends Controller
{
    //
    public function check_login()
    {
        $admin_id=Session::get('admin_id');
        if($admin_id)
        {
             return Redirect::to('/dashboard');
        }else
        {
            return Redirect::to('/admin')->send();
        }
    }
    public function checkout_login()
    {
    	$category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
    	return view('pages.checkout.login_checkout')->with('result_category',$category_product)->with('result_brand',$brand_product);
    }

    public function sign_up_customer(Request $request)
    {
    	$data=array();
    	
    	$data['customer_name']=$request->customer_name;
    	$data['customer_phone']=$request->customer_phone;
    	$data['customer_email']=$request->customer_email;
    	$data['customer_password']=md5($request->customer_password);

    	$customer_id=DB::table('tbl_customer')->insertGetId($data);//vừa insert vừa lấy dữ liệu truyên vào biên này

    	Session::put('customer_id',$customer_id);
    	Session::put('customer_name',$request->customer_name);

    	return Redirect::to('/checkout');

    }
    public function checkout()
    {
    	$category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
    	return view('pages.checkout.checkout_content')->with('result_category',$category_product)->with('result_brand',$brand_product);
    }
    public function save_checkout_customer(Request $request)
    {
    	$data=array();
        
        $data['shipping_name']=$request->shipping_name;
        $data['shipping_address']=$request->shippng_address;
        $data['shipping_phone']=$request->shipping_phone;
        $data['shipping_email']=$request->shipping_email;
        $data['shipping_note']=$request->shipping_note;
        
        $shipping_id=DB::table('tbl_shipping')->insertGetId($data);//vừa insert vừa lấy dữ liệu truyên vào biên này

        Session::put('shipping_id',$shipping_id);
        

        return Redirect::to('/payment');
    }
    public function checkout_logout()
    {
        Session::flush();
        return Redirect::to('/checkout_login');
    }
    public function login_customer(Request $request)
    {
        $email=$request->email_account;
        $password=md5($request->password_account);
        $result=DB::table('tbl_customer')->where('customer_email',$email)->where('customer_password',$password)->first();
        if($result)
        {
            Session::put('customer_id',$result->customer_id);
            Session::put('customer_name',$result->customer_name);
            return Redirect::to('/checkout');
        }else
        {
            return Redirect::to('/checkout_login');
        }
       
    }
    public function payment()
    {
        
        $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
        $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
        return view('pages.checkout.payment')->with('result_category',$category_product)->with('result_brand',$brand_product);
    }
    public function order_place(Request $request)
    {
        //insert tbl_payment
        $data=array();

        $data['payment_method']=$request->payment_option;
        $data['payment_status']='Đang chờ xử lý';
        $payment_id=DB::table('tbl_payment')->insertGetId($data);//vừa insert vừa lấy dữ liệu truyên vào biên này

        //insert tbl_order
        $order_data=array();
        $order_data['customer_id']=Session::get('customer_id');
        $order_data['shipping_id']=Session::get('shipping_id');
        $order_data['payment_id']=$payment_id;
        $order_data['order_total']=Cart::total();
        $order_data['order_status']='Đang chờ xử lý';
        $order_id=DB::table('tbl_order')->insertGetId($order_data);

        //insert tbl_order_details
        $order_dl_data=array();
        $content=Cart::content();
        foreach ($content as $v_content) {
            $order_dl_data['order_id']=$order_id;
            $order_dl_data['product_id']=$v_content->id;
            $order_dl_data['product_name']=$v_content->name;
            $order_dl_data['product_price']=$v_content->price;
            $order_dl_data['product_sales_quantity']=$v_content->qty;
            DB::table('tbl_order_details')->insertGetId($order_dl_data);

        }
        if( $data['payment_method']==1)
        {
            echo 'Thanh toán bằng thẻ ATM';
        }else
        {
            Cart::destroy();
             $category_product=Category::where('category_status','1')->orderBy('category_id','desc')->get();
            $brand_product=Brand::where('brand_status','1')->orderBy('brand_id','desc')->get();
            return view('pages.checkout.handcash')->with('result_category',$category_product)->with('result_brand',$brand_product);
        }

        //return Redirect::to('/payment');
    }
    public function manager_order()
    {
        $this->check_login();
        $list_order=Order::join('tbl_customer','tbl_order.customer_id','=','tbl_customer.customer_id')
        ->select('tbl_order.*','tbl_customer.customer_name')
        ->orderBy('tbl_order.order_id','desc')->get();
        $manager_order=view('admin.manager_order')->with('result_order',$list_order);
        return $manager_order;
    }
    public function view_order($order_id)
    {
       $this->check_login();

       $order_info=Order::
        join('tbl_customer','tbl_order.customer_id','=','tbl_customer.customer_id')
        ->join('tbl_shipping','tbl_order.shipping_id','=','tbl_shipping.shipping_id')
        ->join('tbl_order_details','tbl_order.order_id','=','tbl_order_details.order_id')
        ->where('tbl_order_details.order_id',$order_id)
        ->select('tbl_order.*','tbl_customer.*','tbl_shipping.*','tbl_order_details.*')
        ->first();

        $order_by_id=Order::
        join('tbl_customer','tbl_order.customer_id','=','tbl_customer.customer_id')
        ->join('tbl_shipping','tbl_order.shipping_id','=','tbl_shipping.shipping_id')
        ->join('tbl_order_details','tbl_order.order_id','=','tbl_order_details.order_id')
        ->where('tbl_order_details.order_id',$order_id)
        ->select('tbl_order.*','tbl_customer.*','tbl_shipping.*','tbl_order_details.*')
        ->get();
        $manager_order_by_id=view('admin.view_order')->with('rs_order_by_id',$order_by_id)->with('rs_order_info',$order_info);
        return $manager_order_by_id;
    }
}
