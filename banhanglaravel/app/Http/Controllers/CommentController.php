<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Comment;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
class CommentController extends Controller
{
    //
    public function check_login()
    {
        $admin_id=Session::get('admin_id');
        if($admin_id)
        {
             return Redirect::to('/dashboard');
        }else
        {
            return Redirect::to('/admin')->send();
        }
    }
    public function list_comment()
    {
        $comment=Comment::with('product')->where('comment_parent_content','=',0)->orderBy('comment_status','DESC')->get();
        $comment_rep=Comment::with('product')->where('comment_parent_content','>',0)->orderBy('comment_id','DESC')->get();
        return view('admin.comment.list_comment')->with(compact('comment','comment_rep'));
    }
    public function succes_comment(Request $request)
    {
        $data=$request->all();
        $comment= Comment::find($data['comment_id']);
        $comment->comment_status=$data['comment_status'];
        $comment->save();
    }
    public function active_comment($commentid)
    {
        $this->check_login();
        
        $comment=Comment::find($commentid);
        $comment->comment_status=0;
        $comment->save();
        //Session::put('message','Đã chuyển sang Hiện thành công');
        return Redirect::to('/list_comment');
    }
    public function unactive_comment($commentid)
    {
        $this->check_login();
        
        $comment=Comment::find($commentid);
        $comment->comment_status=1;
        $comment->save();
        //Session::put('message','Đã chuyển sang Hiện thành công');
        return Redirect::to('/list_comment');
    }
    public function reply_comment(Request $request,$commentid)
    {
        $data=$request->all();
        $comment=new Comment();
        // $comment->comment=$data['comment'];
        // $comment->comment_product_id=$data['comment_product_id'];
        // $comment->comment_parent_content=$data['comment_id'];
        // $comment->comment_status=0;
        // $comment->comment_name="EsporeStore";

        $comment->comment=$data['comment_reply'];
        $comment->comment_product_id=$data['comment_product_id'];
        $comment->comment_parent_content=$commentid;
        $comment->comment=$data['comment_reply'];
        $comment->comment_status=0;
        $comment->comment_name="EsporeStore";
        $comment->save();
        Session::put('message','Trả lời bình luận thành công');
        return Redirect::to('/list_comment');
    }
}
