<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    public $timestamps = false; //set time to false
    //có thể insert vào
    protected $fillable = ['comment', 'comment_name','comment_status','comment_date','comment_product_id','comment_parent_content'];
    protected $primaryKey = 'comment_id';
 	protected $table = 'tbl_comment';

 	public function product()
 	{
 		return $this->belongsTo('App\Models\Product','comment_product_id');
 		//1 comment thuộc  về 1 sản phẩm
 	}
}
