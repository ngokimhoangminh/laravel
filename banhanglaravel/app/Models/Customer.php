<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    public $timestamps = false; //set time to false
    //có thể insert vào
    protected $fillable = ['customer_id','customer_name','customer_email','customer_password','customer_phone'];
    protected $primaryKey = 'customer_id';
 	protected $table = 'tbl_customer';
}
