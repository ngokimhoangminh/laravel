<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $timestamps = false; //set time to false
    //có thể insert vào
    protected $fillable = ['category_id', 'brand_id','product_name','product_des','product_content','product_price','product_image','product_status'];
    protected $primaryKey = 'product_id';
 	protected $table = 'tbl_product';

 	public function comment()
 	{
 		return $this->hasMany('App\Models\Comment');
 		//1 sanr phẩm có nhiều comment
 	}
}
