<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//FRONTEND
use App\Http\Controllers\HomeController;
Route::get('/', [HomeController::class, 'index']);
//Route::get('/','Homecontroller@index');
Route::get('/trang-chu','App\Http\Controllers\HomeController@index');

Route::post('/search_product','App\Http\Controllers\HomeController@search');
//Route::get('/trang-chu','Homecontroller@index');

//Home
Route::get('/danh_muc_san_pham/{categoryid}','App\Http\Controllers\CategoryProduct@category_home');
Route::get('/thuong_hieu_san_pham/{brandid}','App\Http\Controllers\CategoryProduct@brand_home');
Route::get('/details_product/{productid}','App\Http\Controllers\ProductController@details_product');
//BACKEND
Route::get('/admin','App\Http\Controllers\Admincontroller@index');
Route::get('/dashboard','App\Http\Controllers\Admincontroller@show_dashboard');
Route::post('/admin-login','App\Http\Controllers\Admincontroller@login');
Route::get('/admin-logout','App\Http\Controllers\Admincontroller@logout');

//Category Product
Route::get('/add_category_product','App\Http\Controllers\CategoryProduct@add_category_product');
Route::post('/save_category_product','App\Http\Controllers\CategoryProduct@save_category_product');

Route::get('/update_category_product/{cat_productid}','App\Http\Controllers\CategoryProduct@update_category_product');
Route::post('/edit_category_product/{cat_productid}','App\Http\Controllers\CategoryProduct@edit_category_product');


Route::get('/delete_category_product/{cat_productid}','App\Http\Controllers\CategoryProduct@delete_category_product');

Route::get('/list_category_product','App\Http\Controllers\CategoryProduct@list_category_product');

Route::get('/unactive_status_category/{cat_productid}','App\Http\Controllers\CategoryProduct@unactive_status_category');
Route::get('/active_status_category/{cat_productid}','App\Http\Controllers\CategoryProduct@active_status_category');

//Brand Product
Route::get('/add_brand_product','App\Http\Controllers\BrandProduct@add_brand_product');
Route::post('/save_brand_product','App\Http\Controllers\BrandProduct@save_brand_product');

Route::get('/update_brand_product/{brand_productid}','App\Http\Controllers\BrandProduct@update_brand_product');
Route::post('/edit_brand_product/{brand_productid}','App\Http\Controllers\BrandProduct@edit_brand_product');


Route::get('/delete_brand_product/{brand_productid}','App\Http\Controllers\BrandProduct@delete_brand_product');

Route::get('/list_brand_product','App\Http\Controllers\BrandProduct@list_brand_product');

Route::get('/unactive_status_brand/{brand_productid}','App\Http\Controllers\BrandProduct@unactive_status_brand');
Route::get('/active_status_brand/{brand_productid}','App\Http\Controllers\BrandProduct@active_status_brand');


//Product

Route::post('/load_comment','App\Http\Controllers\ProductController@load_comment');
Route::post('/send_comment/{productid}','App\Http\Controllers\ProductController@send_comment');
Route::post('/insert_rating','App\Http\Controllers\ProductController@insert_rating');


Route::get('/add_product','App\Http\Controllers\ProductController@add_product');
Route::post('/save_product','App\Http\Controllers\ProductController@save_product');

Route::get('/update_product/{productid}','App\Http\Controllers\ProductController@update_product');
Route::post('/edit_product/{productid}','App\Http\Controllers\ProductController@edit_product');


Route::get('/delete_product/{productid}','App\Http\Controllers\ProductController@delete_product');

Route::get('/list_product','App\Http\Controllers\ProductController@list_product');

Route::get('/unactive_status_product/{productid}','App\Http\Controllers\ProductController@unactive_status_product');
Route::get('/active_status_product/{productid}','App\Http\Controllers\ProductController@active_status_product');

//cart
Route::post('/add_to_cart','App\Http\Controllers\CartController@add_to_cart');
Route::get('/show_cart','App\Http\Controllers\CartController@show_cart');
Route::get('/delete_cart/{rowId}','App\Http\Controllers\CartController@delete_cart');
Route::post('/update_quantity_cart','App\Http\Controllers\CartController@update_quantity_cart');

Route::post('/add-cart-ajax','App\Http\Controllers\CartController@add_cart_ajax');

//check_out
Route::get('/checkout_login','App\Http\Controllers\CheckoutController@checkout_login');
Route::get('/checkout_logout','App\Http\Controllers\CheckoutController@checkout_logout');
Route::post('/sign_up_customer','App\Http\Controllers\CheckoutController@sign_up_customer');
Route::post('/order_place','App\Http\Controllers\CheckoutController@order_place');
Route::post('/login_customer','App\Http\Controllers\CheckoutController@login_customer');
Route::get('/checkout','App\Http\Controllers\CheckoutController@checkout');
Route::post('/save_checkout_customer','App\Http\Controllers\CheckoutController@save_checkout_customer');
Route::get('/payment','App\Http\Controllers\CheckoutController@payment');

//Manager order
Route::get('/manager_order','App\Http\Controllers\CheckoutController@manager_order');
Route::get('/view_order/{order_id}','App\Http\Controllers\CheckoutController@view_order');

//Send Mail
Route::get('/send_mail','App\Http\Controllers\HomeController@send_mail');
//Login facebook
Route::get('/login_facebook','App\Http\Controllers\Admincontroller@login_facebook');
Route::get('/admin/callback','App\Http\Controllers\Admincontroller@callback_facebook');

//Login  google
Route::get('/login-google','App\Http\Controllers\Admincontroller@login_google');
Route::get('/google/callback','App\Http\Controllers\Admincontroller@callback_google');

//Comment
Route::get('/list_comment','App\Http\Controllers\CommentController@list_comment');
//Route::post('/succes_comment','App\Http\Controllers\CommentController@succes_comment');
Route::get('/active_comment/{commentid}','App\Http\Controllers\CommentController@active_comment');
Route::get('/unactive_comment/{commentid}','App\Http\Controllers\CommentController@unactive_comment');
Route::post('/reply_comment/{commentid}','App\Http\Controllers\CommentController@reply_comment');
