@extends('welcome')
@section('content')
<section id="cart_items">
<div class="container" style="width:100%;">
	<div class="breadcrumbs">
		<ol class="breadcrumb">
		  <li><a href="{{URL::to('trang-chu')}}">Trang chủ</a></li>
		  <li class="active">Thanh toán</li>
		</ol>
	</div><!--/breadcrums-->



	<div class="register-req">
		<p>Làm ơn đăng nhập hoặc đăng kí tài khoản để có thể thanh toán và xem lại lịch sử đơn hàng</p>
	</div><!--/register-req-->

	<div class="shopper-informations">
		<div class="row">
			
			<div class="col-sm-12 clearfix">
				<div class="bill-to">
					<p>Điền thông tin đơn hàng</p>
					<div class="form-one">
						<form action="{{URL::to('/save_checkout_customer')}}" method="POST">
							{{csrf_field()}}
							<input type="text" name="shipping_email" placeholder="Email*">						
							<input type="text" name="shipping_name" placeholder="Họ và tên *">
							<input type="text" name="shippng_address" placeholder="Địa chỉ">
							<input type="text" name="shipping_phone" placeholder="Phone number">
							<textarea name="shipping_note"  placeholder="Ghi chú cho đơn hàng của bạn" rows="16"></textarea>
							<input type="submit" value="Gửi" name="save_checkout" class="btn btn-primary btn-sm" style="height: 28px;">
						</form>
					</div>
				
				</div>
			</div>					
		</div>
	</div>
	<div class="review-payment">
		<h2>Kiểm tra lại đơn hàng</h2>
	</div>

	
	<div class="payment-options">
			<span>
				<label><input type="checkbox"> Direct Bank Transfer</label>
			</span>
			<span>
				<label><input type="checkbox"> Check Payment</label>
			</span>
			<span>
				<label><input type="checkbox"> Paypal</label>
			</span>
		</div>
</div>
</section> <!--/#cart_items-->
@endsection 