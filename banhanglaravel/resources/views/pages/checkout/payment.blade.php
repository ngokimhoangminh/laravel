@extends('welcome')
@section('content')
<section id="cart_items">
<div class="container" style="width:100%;">
	<div class="breadcrumbs">
		<ol class="breadcrumb">
		  <li><a href="{{URL::to('trang-chu')}}">Trang chủ</a></li>
		  <li class="active">Thanh toán giỏ hàng</li>
		</ol>
	</div><!--/breadcrums-->

	<div class="review-payment">
		<h2>Kiểm tra lại đơn hàng</h2>
	</div>
		<div class="table-responsive cart_info">
			<?php
				$content=Cart::content();
				//echo '<pre>';
				//	print_r($content);
				//echo '</pre>';
			?>
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Hình ảnh</td>
						<td class="description">Sản phẩm</td>
						<td class="price">Giá</td>
						<td class="quantity">Số lượng</td>
						<td class="total">Tổng tiền</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@foreach($content as $v_content)
					<tr>
						<td class="cart_product">
							<a href=""><img src="{{URL::to('public/backend/uploads/product/'.$v_content->options->image)}}" width="50" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">{{$v_content->name}}</a></h4>
							<p>ID Sản phẩm: {{($v_content->id)}} </p>
						</td>
						<td class="cart_price">
							<p>{{number_format($v_content->price)}}</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button">
								<form action="{{URL::to('/update_quantity_cart')}}" method="POST">
									{{csrf_field()}}
									<input class="cart_quantity_input" type="number" min="1" name="cart_quantity" value="{{$v_content->qty}}">
									<input type="hidden" value="{{$v_content->rowId}}" name="rowid_cart" class="form-control">
									<input type="submit" value="Save" name="update_quantity" class="btn btn-default btn-sm" style="height: 28px;">
								</form>
							</div>
						</td>
						<td class="cart_total" >
							<p class="cart_total_price">
								<?php
									$total_money=$v_content->price * $v_content->qty;
									echo number_format($total_money).' '.'VND';

								?>
							</p>
						</td>
						<td class="cart_delete">
							<a class="cart_quantity_delete" href="{{URL::to('/delete_cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	<h4 style="margin: 40px 0px; font-size: 20px;">Chọn hình thức thanh toán</h4>
	<form method="POST" action="{{URL::to('/order_place')}}">
		{{csrf_field()}}
		<div class="payment-options">
				<span>
					<label><input  name="payment_option" type="checkbox" value="1">Thanh toán bằng thẻ ATM</label>
				</span>
				<span>
					<label><input  name="payment_option" type="checkbox" value="2">Thanh toán khi nhận hàng</label>
				</span>
				{{-- <span>
					<label><input type="checkbox"> Paypal</label>
				</span> --}}
				<input type="submit" value="Đặt hàng" name="send_order_place" class="btn btn-primary btn-sm" style="height: 28px;">
		</div>

	</form>
</div>
</section> <!--/#cart_items-->
@endsection 