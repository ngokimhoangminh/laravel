@extends('welcome')
@section('content')
@foreach($result_dl_product as $key => $dl_product)
<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="{{URL::to('/public/backend/uploads/product/'.$dl_product->product_image)}}" alt="" />
			<h3>ZOOM</h3>
		</div>
		<div id="similar-product" class="carousel slide" data-ride="carousel">
			
			  <!-- Wrapper for slides -->
			    <div class="carousel-inner">
					<div class="item active">
					  <a href=""><img src="{{URL::to('public/frontend/image/similar1.jpg')}}" alt=""></a>
					  <a href=""><img src="{{URL::to('public/frontend/image/similar2.jpg')}}" alt=""></a>
					  <a href=""><img src="{{URL::to('public/frontend/image/similar3.jpg')}}" alt=""></a>
					</div>
				</div>

			  <!-- Controls -->
			  <a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>
		</div>

	</div>
	<div class="col-sm-7">
		<div class="product-information"><!--/product-information-->
			
			<h2>{{($dl_product->product_name)}}</h2>
			<p>ID san pham: {{($dl_product->product_id)}}</p>
			<img src="images/product-details/rating.png" alt="" />
			<form action="{{URL::to('/add_to_cart')}}" method="POST">
				{{csrf_field()}}
				<span>
					<span>{{number_format($dl_product->product_price).' '.'VND'}}</span>
					<label>So luong:</label>
					<input name="quantity" type="number" min="1" value="1" />
					<input name="product_id_hidden" type="hidden" value="{{($dl_product->product_id)}}" />
					<button type="submit" class="btn btn-fefault cart mt-2">
						<i class="fa fa-shopping-cart"></i>
						Add to cart
					</button>			
				</span>
			</form>
			<p><b>Tình trạng: </b>Còn hàng </p>
			<p><b>Mô tả: </b>{{($dl_product->product_des)}}</p>
			<p><b>Brand: </b>{{($dl_product->brand_name)}}</p>
			<a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
		</div><!--/product-information-->
	</div>
</div><!--/product-details-->

<div class="category-tab shop-details-tab"><!--category-tab-->
	<div class="col-sm-12">
		<ul class="nav nav-tabs">
			<li ><a href="#details" data-toggle="tab">Chi tiết</a></li>
			<li class="active" ><a href="#reviews" data-toggle="tab">Đánh giá</a></li>
		</ul>
	</div>
	<div class="tab-content ">
		<div class="tab-pane fade " id="details" >
			<div class="col-sm-3">
				<p>Nội dung sản phẩm: </p>
				<p><b>{!! $dl_product->product_content !!}</b></p>
				
			</div>

		</div>
		<div class="tab-pane fade active in" id="reviews" >
			<div class="col-sm-12">
				<ul>
					<li><a href=""><i class="fa fa-user"></i>Đà Nẵng</a></li>
					<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
					<li><a href=""><i class="fa fa-calendar-o"></i>23 FEB 2021</a></li>
				</ul>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<style type="text/css">
					.style_comment{
						border: 1px solid #ddd;
						border-radius: 10px;
						background: #f0f0e9;
						display: flex;
						align-items: center;
					}
				</style>
				<form>
					@csrf
					
					<input type="hidden" class="comment_product_id" name="comment_product_id" value="{{$dl_product->product_id}}"> 
					<div id="comment_show"></div>
					
				</form>
				<p class="mt-2"><b>Viết bình luận của bạn</b></p>
				
				<ul class="list-inline" title="Average Rating">
					@for($count=1;$count<=5;$count++)
						@php
							if($count<=$rating)
							{
								$color='color:#FFA500;';
							}else
							{
								$color='color:#ccc;';
							}
						@endphp
					<li title="Đánh giá sao"
						id="{{$dl_product->product_id}}-{{$count}}"
						data-index="{{$count}}"
						data-product_id="{{$dl_product->product_id}}"
						data-rating="{{$rating}}"
						class="rating"
						style="cursor: pointer; font-size:30px; {{$color}}">
						&#9733;
					</li>
					@endfor
				</ul>
				
				<form action="{{URL::to('/send_comment/'.$dl_product->product_id)}}" method="POST" >
					{{csrf_field()}}
					<span>
					
					{{-- <input style="width:100%; margin-left:0" type="text" name="comment_name" class="comment_name" placeholder="Tên bình luận"/> --}}
					<input type="hidden" class="comment_product_id" name="comment_product_id" value="{{$dl_product->product_id}}"> 	
					</span>
					<div class="row" style="display: flex;
						align-items: center;color: ">	
						<div class="col-md-1">
                            
                            <a href="#"><img  class="img img-thumbnail" src="{{URL::to('public/frontend/image/3.png')}}"></a>
                    	</div>
                    	<div class="col-md-11">
                    		<textarea rows="3" style="height: 80px;" name="comment_content" class="comment_content" placeholder="Nhập nội dung bình luận" ></textarea>
                    	</div>
					</div>
					
					<b>Đánh giá : {{$rating}}/5 </b> <ul class="list-inline" title="Average Rating">
					@for($count=1;$count<=5;$count++)
						@php
							if($count<=$rating)
							{
								$color='color:#FFA500;';
							}else
							{
								$color='color:#ccc;';
							}
						@endphp
					<li title="Đánh giá sao"
						id="{{$dl_product->product_id}}-{{$count}}"
						data-index="{{$count}}"
						data-product_id="{{$dl_product->product_id}}"
						data-rating="{{$rating}}"
						class="rating"
						style="cursor: pointer; font-size:20px; {{$color}}">
						&#9733;
					</li>
					@endfor
				</ul>
					<button type="submit" class="btn btn-default pull-right send_comment">
						Đăng
					</button>
					<div id="notify_comment">
						<?php
					        $message=Session::get('message');
					        if($message)
					        {
					          echo '<span class="text-success">'.$message.'</span>';
					          Session::put('message',null);
					        }
      					?>
					</div>
				</form>
			</div>
		</div>
		
	</div>
</div><!--/category-tab-->
@endforeach
<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">recommended items</h2>
	
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
			@foreach($re_product as $key => $re_value)	
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							 <div class="productinfo text-center">
                                <img src="{{URL::to('public/backend/uploads/product/'.$re_value->product_image)}}" alt="" />
                                <h2>{{number_format($re_value->product_price).' '.'VND'}}</h2>
                                <p>{{$re_value->product_name}}</p>
                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                            </div>
						</div>
					</div>
				</div>
			@endforeach
				
			</div>
			
		</div>
		 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		  </a>
		  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		  </a>			
	</div>
</div><!--/recommended_items-->
@endsection                 