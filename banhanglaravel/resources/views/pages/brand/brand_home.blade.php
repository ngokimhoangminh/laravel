@extends('welcome')
@section('content')
<div class="features_items"><!--features_items-->
                        @foreach($result_brand_name as $key => $name_brand)
                            <h2 class="title text-center">{{$name_brand->brand_name}}</h2>
                        @endforeach
                         @foreach($rs_brand_home as $key => $product)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <form>
                                                @csrf

                                                <input type="hidden" class="cart_product_id_{{$product->product_id}}" value="{{$product->product_id}}">
                                                <input type="hidden" class="cart_product_name_{{$product->product_id}}" value="{{$product->product_name}}">
                                                <input type="hidden" class="cart_product_price_{{$product->product_id}}" value="{{$product->product_price}}">
                                                <input type="hidden" class="cart_product_image_{{$product->product_id}}" value="{{$product->product_image}}">
                                                <input type="hidden" class="cart_product_qty_{{$product->product_id}}" value="{{$product->product_qty}}">

                                                <img src="{{URL::to('public/backend/uploads/product/'.$product->product_image)}}" alt="" />
                                                <h2>{{number_format($product->product_price).' '.'VND'}}</h2>
                                                <p>{{$product->product_name}}</p>
                                                <a href="{{URL::to('/details_product/'.$product->product_id)}}" class="btn btn-default add-to-cart">
                                                        <i class="fa fa-shopping-cart"></i>Xem chi tiết</a>
                                                <button type="button" class="btn btn-default add-to-cart" name="add-to-cart" data-id_product="{{$product->product_id}}">
                                                    <i class="fa fa-shopping-cart"></i>Add to Cart
                                                </button>
                                            {{-- <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a> --}}
                                            </form>
                                        </div>
                                        
                                        {{-- <div class="product-overlay">
                                            <div class="overlay-content">
                                                <h2>{{number_format($product->product_price).' '.'VND'}}</h2>
                                                <p>{{$product->product_name}}</p>
                                                <a href="{{URL::to('/details_product/'.$product->product_id)}}" class="btn btn-default add-to-cart">
                                                    <i class="fa fa-shopping-cart"></i>Xem chi tiết</a>
                                               
                                            </div>
                                        </div> --}}
                                        
                                </div>
                                
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="#"><i class="fa fa-plus-square"></i> Yêu thích</a></li>
                                        <li><a href="#"><i class="fa fa-plus-square"></i>Thêm so sánh</a></li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                        @endforeach
</div><!--features_items-->

@endsection                 