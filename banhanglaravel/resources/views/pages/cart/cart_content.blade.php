@extends('welcome')
@section('content')
<section id="cart_items">
	<div class="container" style="
    width: 100%;">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="{{URL::to('/trang-chu')}}">Trang chủ</a></li>
			  <li class="active">Giỏ hàng</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
			<?php
				$content=Cart::content();
				//echo '<pre>';
				//	print_r($content);
				//echo '</pre>';
			?>
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Hình ảnh</td>
						<td class="description">Sản phẩm</td>
						<td class="price">Giá</td>
						<td class="quantity">Số lượng</td>
						<td class="total">Tổng tiền</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@foreach($content as $v_content)
					<tr>
						<td class="cart_product">
							<a href=""><img src="{{URL::to('public/backend/uploads/product/'.$v_content->options->image)}}" width="50" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">{{$v_content->name}}</a></h4>
							<p>ID Sản phẩm: {{($v_content->id)}} </p>
						</td>
						<td class="cart_price">
							<p>{{number_format($v_content->price)}}</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button">
								<form action="{{URL::to('/update_quantity_cart')}}" method="POST">
									{{csrf_field()}}
									<input class="cart_quantity_input" type="number" min="1" name="cart_quantity" value="{{$v_content->qty}}">
									<input type="hidden" value="{{$v_content->rowId}}" name="rowid_cart" class="form-control">
									<input type="submit" value="Save" name="update_quantity" class="btn btn-default btn-sm" style="height: 28px;">
								</form>
							</div>
						</td>
						<td class="cart_total" >
							<p class="cart_total_price">
								<?php
									$total_money=$v_content->price * $v_content->qty;
									echo number_format($total_money).' '.'VND';

								?>
							</p>
						</td>
						<td class="cart_delete">
							<a class="cart_quantity_delete" href="{{URL::to('/delete_cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section> <!--/#cart_items-->

<section id="do_action">
	<div class="container" style="
    width: 100%;">
		<div class="heading">
			<h3>What would you like to do next?</h3>
			<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li>
							<input type="checkbox">
							<label>Use Coupon Code</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Use Gift Voucher</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Estimate Shipping & Taxes</label>
						</li>
					</ul>
					<ul class="user_info">
						<li class="single_field">
							<label>Country:</label>
							<select>
								<option>United States</option>
								<option>Bangladesh</option>
								<option>UK</option>
								<option>India</option>
								<option>Pakistan</option>
								<option>Ucrane</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
							
						</li>
						<li class="single_field">
							<label>Region / State:</label>
							<select>
								<option>Select</option>
								<option>Dhaka</option>
								<option>London</option>
								<option>Dillih</option>
								<option>Lahore</option>
								<option>Alaska</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
						
						</li>
						<li class="single_field zip-field">
							<label>Zip Code:</label>
							<input type="text">
						</li>
					</ul>
					<a class="btn btn-default update" href="">Get Quotes</a>
					<a class="btn btn-default check_out" href="">Continue</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="total_area">
					<ul>
						<li>Tổng tiền: <span>{{Cart::subtotal().' '.'VND'}}</span></li>
						<li>Phí Thuế: <span>{{Cart::tax().' '.'VND'}}</span></li>
						<li>Phí vận chuyển <span>Free</span></li>
						<li>Thành tiền: <span>{{Cart::total().' '.'VND'}}</span></li>
					</ul>
				      <?php
                                    $customer_id=Session::get('customer_id');
                                    $shipping_id=Session::get('shipping_id');
                                    if($customer_id!=NULL && $shipping_id==NULL)
                                    {

                                ?>
                                     <a class="btn btn-default check_out" href="{{URL::to('/checkout')}}">Check Out</a>
                                <?php
                            }elseif($customer_id!=NULL && $shipping_id!=NULL)
                            {
                            ?>
                             <a class="btn btn-default check_out" href="{{URL::to('payment')}}">Check Out</a>
                            <?php
                            }else{
                            ?>
                                <a class="btn btn-default check_out" href="{{URL::to('/checkout_login')}}">Check Out</a>
                            <?php
                            }
                            ?>
						
				</div>
			</div>
		</div>
	</div>
</section><!--/#do_action-->
@endsection 
