@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Cập Nhật Thương Hiệu Sản Phẩm
                </header>
                <?php
                        $message=Session::get('message');
                        if($message)
                        {
                            echo '<span class="text-success">'.$message.'</span>';
                            Session::put('message',null);
                        }
                ?>
                <div class="panel-body">
                   @foreach($result_update as $key => $update_value)
                    <div class="position-center">
                        <form role="form" action="{{URL::to('edit_brand_product/'.$update_value->brand_id)}}" method="POST">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Thương Hiệu</label>
                            <input type="text" value="{{$update_value->brand_name}}" name="brand_product_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Category">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung thương hiệu</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" name="brand_product_des">{{$update_value->brand_des}}</textarea>
                        </div>
   
                        <button type="submit" class="btn btn-info" name="update_brand_product">Cập nhật</button>
                    </form>
                    </div>
                @endforeach
                </div>
                {{-- <div class="panel-body">
                  
                    <div class="position-center">
                        <form role="form" action="{{URL::to('edit_brand_product/'.$result_update->brand_id)}}" method="POST">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Thương Hiệu</label>
                            <input type="text" value="{{$result_update->brand_name}}" name="brand_product_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Category">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung thương hiệu</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" name="brand_product_des">{{$result_update->brand_des}}</textarea>
                        </div>
   
                        <button type="submit" class="btn btn-info" name="update_brand_product">Cập nhật</button>
                    </form>
                    </div>
               
                </div> --}}
            </section>

    </div>
   
</div>
@endsection