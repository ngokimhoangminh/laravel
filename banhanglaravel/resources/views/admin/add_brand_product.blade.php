@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Thêm Thương Hiệu Sản Phẩm
                </header>
                <div class="panel-body">
                    <?php
                        $message=Session::get('message');
                        if($message)
                        {
                            echo '<span class="text-success">'.$message.'</span>';
                            Session::put('message',null);
                        }
                    ?>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('save_brand_product')}}" method="POST">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Thương Hiệu</label>
                            <input type="text" name="brand_product_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Brand">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung Thương Hiệu</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" placeholder="Thông tin thương hiệu" name="brand_product_des"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái</label>
                            <select name="brand_product_status" class="form-control input-sm m-bot15">
                                <option value="1">Hiện</option>
                                <option value="0">Ẩn</option>
                                
                            </select>
                        </div>
                        
                        <button type="submit" class="btn btn-info" name="add_brand_product">Thêm</button>
                    </form>
                    </div>

                </div>
            </section>

    </div>
   
</div>
@endsection