@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Cập Nhật Danh Mục Sản Phẩm
                </header>
                <?php
                        $message=Session::get('message');
                        if($message)
                        {
                            echo '<span class="text-success">'.$message.'</span>';
                            Session::put('message',null);
                        }
                ?>
                <div class="panel-body">
                  
                    <div class="position-center">
                        <form role="form" action="{{URL::to('edit_category_product/'.$result_update->category_id)}}" method="POST">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Danh Mục</label>
                            <input type="text" value="{{$result_update->category_name}}" name="category_product_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Category">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung danh mục</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" name="category_product_des">{{$result_update->category_des}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Từ khóa danh mục</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" name="meta_category_keywords">{{$result_update->meta_keywords}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-info" name="update_category_product">Cập nhật</button>
                    </form>
                    </div>
                
                </div>
            </section>

    </div>
   
</div>
@endsection