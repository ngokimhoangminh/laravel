@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Cập Nhật Sản Phẩm
                </header>
                <?php
                        $message=Session::get('message');
                        if($message)
                        {
                            echo '<span class="text-success">'.$message.'</span>';
                            Session::put('message',null);
                        }
                    ?>
                <div class="panel-body">
                    
                    <div class="position-center">
                        <form role="form" action="{{URL::to('edit_product/'.$result_update->product_id)}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên sản phẩm</label>
                            <input type="text" name="product_name" class="form-control" id="exampleInputEmail1" value="{{$result_update->product_name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                            <select name="product_category" class="form-control input-sm m-bot15">
                                @foreach($result_category as $key => $cat)
                                    @if($cat->category_id==$result_update->category_id)
                                        <option selected value="{{$cat->category_id}}">{{$cat->category_name}}</option>
                                    @else
                                        <option  value="{{$cat->category_id}}">{{$cat->category_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Thương hiệu</label>
                            <select name="product_brand" class="form-control input-sm m-bot15">

                                @foreach($result_brand as $key => $brand)
                                     @if($brand->brand_id==$result_update->brand_id)
                                        <option selected value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>   
                                    @else
                                        <option value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
                                    @endif   
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả sản phẩm</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1"name="product_des">{{$result_update->product_des}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung sản phẩm</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" name="product_content">{{$result_update->product_content}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá sản phẩm</label>
                            <input type="text" name="product_price" value="{{$result_update->product_price}}" class="form-control" id="exampleInputEmail1" >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình ảnh</label>
                            <img src="{{URL::to('public/backend/uploads/product/'.$result_update->product_image)}}" width="250" height="150">
                            <input type="file" name="product_image" value="" class="form-control" id="exampleInputEmail1">

                        </div>
                        
                        <button type="submit" class="btn btn-info" name="add_product">Cập nhật</button>
                    </form>
                    </div>
                    
                </div>
            </section>

    </div>
   
</div>
@endsection