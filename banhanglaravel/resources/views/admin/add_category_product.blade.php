@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Thêm Danh Mục Sản Phẩm
                </header>
                <div class="panel-body">
                    <?php
                        $message=Session::get('message');
                        if($message)
                        {
                            echo '<span class="text-success">'.$message.'</span>';
                            Session::put('message',null);
                        }
                    ?>
                    <div class="position-center">
                        <form role="form" action="{{URL::to('save_category_product')}}" method="POST">
                            {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Danh Mục</label>
                            <input type="text" name="category_product_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Category">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nội dung danh mục</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" placeholder="Thông tin danh mục" name="category_product_des"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Từ khóa danh mục</label>
                            <textarea style="resize:none" rows="5" class="form-control" id="exampleInputPassword1" placeholder="Thông tin danh mục" name="meta_category_keys"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái</label>
                            <select name="category_product_status" class="form-control input-sm m-bot15">
                                <option value="1">Hiện</option>
                                <option value="0">Ẩn</option>
                                
                            </select>
                        </div>
                        
                        <button type="submit" class="btn btn-info" name="add_category_product">Thêm</button>
                    </form>
                    </div>

                </div>
            </section>

    </div>
   
</div>
@endsection