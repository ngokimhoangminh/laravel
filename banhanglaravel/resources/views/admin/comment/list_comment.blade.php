@extends('admin_layout')
@section('admin_content')
<div class="table-agile-info">
  <div class="panel panel-default">
    <div class="panel-heading">
      Liệt Kê Bình Luận
    </div>

    <div id="notify_comment"></div>
    <div class="row w3-res-tb">
      <div class="col-sm-5 m-b-xs">
        <select class="input-sm form-control w-sm inline v-middle">
          <option value="0">Bulk action</option>
          <option value="1">Delete selected</option>
          <option value="2">Bulk edit</option>
          <option value="3">Export</option>
        </select>
        <button class="btn btn-sm btn-default">Apply</button>                
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-3">
        <div class="input-group">
          <input type="text" class="input-sm form-control" placeholder="Search">
          <span class="input-group-btn">
            <button class="btn btn-sm btn-default" type="button">Go!</button>
          </span>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <?php
        $message=Session::get('message');
        if($message)
        {
          echo '<span class="text-success">'.$message.'</span>';
          Session::put('message',null);
        }
      ?>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            
            <th>Bình Luận</th>
            <th>Người Bình Luận</th>
            <th>Duyệt</th>
            <th>Sản Phẩm</th>
            <th>Đánh Giá</th>
            
          </tr>
        </thead>
        <tbody>
          @foreach($comment as $key => $comm)
          <tr>
            
            <td>{{$comm->comment}}
              <style type="text/css">
                ul.list_rep li{
                  list-style-type: decimal;
                  margin: 5px 40px;
                  color: #286090;
                }
              </style>
              <ul class="list_rep">
                @foreach($comment_rep as $key => $com_reply)
                  @if($com_reply->comment_parent_content==$comm->comment_id)
                    Trả lời: <li>{{$com_reply->comment}}</li>
                  @endif
                @endforeach
              </ul>
              @if($comm->comment_status==0) 
                <form action="{{URL::to('/reply_comment/'.$comm->comment_id)}}" method="POST">
                  {{csrf_field()}}
                  <br/><textarea class="form-control reply_comment" name="comment_reply" rows="5"></textarea>
                  <input type="hidden" name="comment_product_id" value="{{$comm->comment_product_id}}">
                  <br/><button class="btn btn-default btn-xs btn_reply_comment">Trả lời bình luận</button>
                </form>
                {{-- <button class="btn btn-default btn-xs btn_reply_comment" data-product_id="{{$comm->comment_product_id}}" data-comment_id="{{$comm->comment_id}}" >Trả lời bình luận</button> --}}
              @endif
              
            </td>
            <td>{{$comm->comment_name}}</td>
            <td>
              @if($comm->comment_status==1)
               
              <form action="{{URL::to('/active_comment/'.$comm->comment_id)}}" >
                <button type="submit" class="btn btn-primary btn-xs">Duyệt</button>
                  
              </form> 
                {{-- <input type="button" data-comment_status="0" data-comment_id="{{$comm->comment_id}}" id="{{$comm->comment_product_id}}" class="btn btn-primary btn-xs comment_duyet_btn" value="Duyệt"> --}}
              @else
               
                {{-- <input type="button" data-comment_status="1" {{URL::to('/unactive_comment/'.$comm->comment_id)}}data-comment_id="{{$comm->comment_id}}" id="{{$comm->comment_product_id}}" class="btn btn-danger btn-xs comment_duyet_btn" value="Bỏ Duyệt"> --}}
                 <form action="{{URL::to('/unactive_comment/'.$comm->comment_id)}}" >
                    <button type="submit" class="btn btn-danger btn-xs">Bỏ Duyệt</button>
                </form>
              @endif

            </td>
            <td><a href="{{URL::to('/details_product/'.$comm->comment_product_id)}}" target="blank">{{$comm->product->product_name}}</a></td>
            <td>
              <a href="" class="active styling-edit" ui-toggle-class><i class="fa fa-pencil-square-o text-success text-active"></i></a>
              <a onclick="return confirm('Are you sure to delete?')" href="" class="active styling-edit" ui-toggle-class>
                <i class="fa fa-times text-danger text"></i>
              </a>
            </td>
          </tr>
         @endforeach
        </tbody>
      </table>
    </div>
    
  </div>
</div>
@endsection